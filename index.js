/**
 * Bài tập 1
 * Input: nhập số nguyên x
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến sum = 0
 *    + Dùng vòng lặp for(var i = 0; sum < 10000; i++), với mỗi vòng lặp thực hiện lệnh:
 *          Gán biến sum += i
 *    +In kết quả i-1 ra giao diện web       
 * 
 * Output: in ra các số nguyên tố từ 1 tới giá trị của x
 */
 
function baiTap5() {
  var soX = document.getElementById("so-x").value * 1;
  var check;
  var soNguyenToArr = [];
  if (soX < 2) {
    document.getElementById("result-b5").innerText = `Dữ liệu không hợp lệ (hãy nhập số lớn hơn 1)`;
  }
  else {
    for (var i = 2; i <= soX; i++) {
      check = true;
      for (var k = 2; k <= Math.sqrt(i); k++) {
        if (i % k == 0) {
          check = false; 
        }
      }
      if (check) {
        soNguyenToArr.push(i);
      }
    }
    var inSo = soNguyenToArr.join(", ");
    document.getElementById("result-b5").innerHTML = `Các số nguyên tố: ${inSo}`;
  }
  
}
